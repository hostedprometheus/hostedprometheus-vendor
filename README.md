hostedprometheus-vendor
=======================

Problem: prometheus is too crazy with dependencies to use a dependency
management tool so it's easier to just copy the project and its vendor directory
into the hostedprometheus vendor directory, but I also hate committing huge 
piles of vendor code where the purpose of version control is to focus on YOUR
problem, not version-controlling line-by-line all your dependencies... so...
submodules seem like the lesser evil.
